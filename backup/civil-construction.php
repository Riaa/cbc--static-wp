<?php 
  include('header.php');
?>
    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Civil Construction</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Civil Construction <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-services ftco-no-pt">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <br/><br/>
            <span class="subheading">Civil Construction</span>
            <h2 class="mb-4">Our Services</h2>
            <p>The Chittagong Builders Corporation (CBC) is a famous and renowned Construction company in Bangladesh.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-hook"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Brick Work</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-skyline"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Paving Installation</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-stairs"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Plastering, Floors & Screeding</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-home"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Tiling</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-home"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Plumbering</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-home"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Painting</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-3 d-flex services align-self-stretch p-4 ftco-animate">
            <div class="media block-6 d-block text-center">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-home"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Bulk Earth Works & Plant Hire</h3>
                <p>Creating an environment for innovation and quest to render the world class services.</p>
              </div>
            </div>      
          </div>


        </div>
      </div>
    </section>
    
   <?php 
    include('footer.php');
  ?>