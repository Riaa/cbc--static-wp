<?php 
  include('header.php');
?>
    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Industrial Works Portfolio</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Industrial Works Portfolio <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-section ftco-no-pt ftco-no-pb">
      <div class="container-fluid p-0">
        <div class="row no-gutters justify-content-center mb-5 pb-2">
          <div class="col-md-6 text-center heading-section ftco-animate">
            <br/><br/>
            <span class="subheading">Projects</span>
            <h2 class="mb-4">Industrial Works Portfolio</h2>
            <p>we determine to expand our services widely to the Industrial sector in Bangladesh with modern effective evaluation</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-1.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-2.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-2.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-3.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-3.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-4.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-4.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-5.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-5.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-6.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Resedencial</span>
                <h3><a href="project.html">Rose Villa House</a></h3>
              </div>
              <a href="images/work-6.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-7.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-7.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 ftco-animate">
            <div class="project">
              <img src="images/work-8.jpg" class="img-fluid" alt="Colorlib Template">
              <div class="text">
                <span>Commercial</span>
                <h3><a href="project.html">San Francisco Tower</a></h3>
              </div>
              <a href="images/work-8.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                <span class="icon-expand"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    
   <?php 
    include('footer.php');
  ?>