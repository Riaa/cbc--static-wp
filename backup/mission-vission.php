<?php 
  include('header.php');
?>
    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Mission & Vission</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Mission & Vission <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
			<div class="container">
				<div class="row no-gutters">
          <div class="col-md-6 wrap-about py-5 px-4 px-md-5 ftco-animate">
            <div class="heading-section mb-5">
              <h2 class="mb-4">Our Mission</h2>
            </div>
            <div class="">
              <p class="text-justify">Chittagong Builders Corporation intends to improve the standard of
              living by empowering disadvantaged communities by providing employment
              and skills development to people in the communities where
              Tenders are awarded.<br/><br/> It is also our mission to improve the standard of
              civil and construction quality in our country by using skilled and competitive
              employees.</p>
            </div>
          </div>
          <div class="col-md-6 wrap-about py-5 px-4 px-md-5 ftco-animate">
            <div class="heading-section mb-5">
              <h2 class="mb-4">Our Vission</h2>
            </div>
            <div class="">
              <p class="text-justify">The vision of Chittagong Builders Corporation is to provide quality services
              in civil and construction industries, with safe methods at the
              cost effective to the client.<br/><br/> We aim to create employment and skills
              development in communities we’re tasked to work in and to improve
              the standard of construction by building to expected quality using
              safe methods of construction, giving value to the money of the client.</p>
            </div>
          </div>
        </div>
        <div class="row no-gutters">
					<div class="col-md-6 wrap-about py-5 px-4 px-md-5 ftco-animate">
            <div class="heading-section mb-5">
              <h2 class="mb-4">Our Values</h2>
            </div>
            <div class="">
              <p class="text-justify">Creating an environment for innovation and quest to render the world
              class services. A belief in a lateral leadership style where an environment
              of trust forms the foundation of the company.A belief that loyalty
              provides sustainability for a secure future for its people and members.</p>
            </div>
          </div>
          <div class="col-md-6 wrap-about py-5 px-4 px-md-5 ftco-animate">
	          <div class="heading-section mb-5">
	            <h2 class="mb-4">Objectives & Goals</h2>
	          </div>
	          <div class="">
							<ol type="disc" class="text-justify">
                <li>Make sure safety in working place</li>
                <li>To share quality business development in Bangladesh</li>
                <li>Qualified and well trained Engineers, Staff, workers etc.</li>
                <li>Ensure Health & Safety training for all workers</li>
                <li>Work for society.</li>
              </ol>
						</div>
					</div>
				</div>

			</div>
		</section>
    
   <?php 
    include('footer.php');
  ?>