<?php
function cbc_function(){
	//adding navwalker file
	require_once get_template_directory(). '/wp-bootstrap-navwalker.php';
	
	//custom title 
	add_theme_support('title-tag');


	//thumbnail
	add_theme_support('post-thumbnails');

	//custom  backround
	add_theme_support('custom-backround');

	//custom logo
	add_theme_support('custom-logo',array(
		'defualt-image' =>  get_template_directory_uri().'/images/cbc-logo.png',
	));
	//load theme textdomain
	load_theme_textdomain('cbc',get_template_directory_uri().'/languages');


	//register menus
	if(function_exists('register_nav_menus')){
	register_nav_menus(array(
	'primarymenu' 		=>__('Header Menu','cbc'),
	'secondarymenu'		=>__('Footer Menu','cbc')
		));
	}
//read more
		function read_more($limit){
		$post_content = explode(" ", get_the_content());

		$less_content = array_slice($post_content, 0, $limit);

		echo implode(" ", $less_content);
	}












}

add_action('after_setup_theme','cbc_function');



function cbc_widgets(){

	register_sidebar(array(
		'name' => __('Footer Widgets','cbc'),
		'description' => __('Add your footer widgets here','cbc'),
		'id' => 'footer-widget',
		'before_widget' => '<div class="col-md-3"><div class="ftco-footer-widget mb-6">',
		'after_widget' => '</p></div></div>',
		'before_title' => '<h2 class="ftco-heading-2">',
		'after_title' => '</h2><p>'

	));


}
add_action('widgets_init','cbc_widgets');


?>






