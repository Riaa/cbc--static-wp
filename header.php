<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  	
  
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/animate.css">
    
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/aos.css">

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/flaticon.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/icomoon.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /css/style-2.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri());?> /style.css">
    <?php wp_head(); ?>
  </head>
  <body>
    <div class="bg-top navbar-light">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-center align-items-stretch">
    			<div class="col-md-4 d-flex align-items-center">
    				<div class="row lrow">
    					<div class="col-md-12">
    				<!--<a class="navbar-brand" href="index.php">
    					<img src="<?php //echo esc_url(get_template_directory_uri());?> /images/cbc-logo.png" width="auto" height="auto" alt="Logo" />
    				</a>
    			-->
    			<?php
    				if(function_exists('the_custom_logo')){
    					the_custom_logo();
    				}
    			?>
	    				</div>
	    			</div>
    			</div>
	    		<div class="col-lg-8 d-block lt">
	    			<div class="row">
	    				<div class="col-md-9"></div>

	    				<div class="col-md-3 tcon">
		    				<p class="">
		    					<span class="icon-paper-plane"></span> info@cbc.com.bd<br/>
		    					<span class="icon-phone2"></span> +88 01819-322295
		    				</p>
			    		</div>
	    			</div>
		    		<!--<div class="row d-flex ths">
					    <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
					    	<div class="icon d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
					    	<div class="text d-flex align-items-center">
						    	<span>info@cbc.com.bd</span>
						    </div>
					    </div>

					    <div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
					    	<div class="icon d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <div class="text d-flex align-items-center">
						    	<span>
						    	 +88 01819-322295</span>
						    </div>
					    </div>
					    <div class="col-md topper d-flex align-items-center align-items-stretch">
					    	<div class="ftco-footer-widget tp">
				              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
				                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
				                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
				                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
				              </ul>
				            </div>
					    </div>
				    </div>-->
			    </div>
		    </div>
		  </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light">
    	<div class="container">
		  <!--<a class="navbar-brand" href="#">CBC</a>-->
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="" id="">
		<?php
		  	if(function_exists('wp_nav_menu')){
		  		wp_nav_menu(array(
		  			'theme_location'	=> 'primarymenu',
		  			'depth'				=> '2', //1= no dropdown & 2= dropdown.
		  			'container'			=> 'div',
		  			'container_class'	=> 'collapse navbar-collapse',
		  			'container_id'		=> 'navbarSupportedContent',
		  			'menu_class'		=> 'navbar-nav mr-auto',
		  			'fallback_cb'		=> 'wp_Bootstrap_Navwalker::fallback',
		  			'walker'			=> new WP_Bootstrap_Navwalker(),
		  		));
		  	}
		?>

		   <!-- <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Company
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="about.php">About Us</a>
		          <a class="dropdown-item" href="mission-vission.php">Mission & Vission</a>
		          <a class="dropdown-item" href="company-profile.php">Company Profile</a>				  
		        </div>
		      </li>

		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Services
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				  <a class="dropdown-item" href="civil-construction.php">Civil Construction</a>
				  <a class="dropdown-item" href="industrial-fabrication.php">Industrial Fabrication</a>
				  <a class="dropdown-item" href="mechanical-works.php">Mechanical Works</a>
		        </div>
		      </li>
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Portfolio
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				  <a class="dropdown-item" href="pt-civil.php">Civil Construction</a>
				  <a class="dropdown-item" href="pt-industrial.php">Industrial Fabrication</a>
				  <a class="dropdown-item" href="pt-mechanical.php">Mechanical Works</a>
		        </div>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="contact.php">Contact</a>
		      </li>
		    </ul>-->
		  </div>
  		</div>
	</nav>
	